FROM google/cloud-sdk:alpine

ARG HELM_URL=https://get.helm.sh
ARG HELM_TARBALL=helm-v2.12.0-linux-amd64.tar.gz

# Update alpine packages
RUN apk update && apk add --no-cache openssl && \
    rm -rf /var/cache/apk/*

# Install Helm
RUN wget -q $HELM_URL/$HELM_TARBALL -P /tmp && \
    mkdir -p /tmp/helm && tar xzfv /tmp/$HELM_TARBALL -C /tmp/helm && \
    mv /tmp/helm/linux-amd64/helm /usr/bin/helm

# Install extra gcloud components
RUN gcloud --quiet components install beta kubectl gke-gcloud-auth-plugin

USER 1000:1000
