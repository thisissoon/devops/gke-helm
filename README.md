# GKE + Helm Deploy Image

Docker image to run deployments to GKE with Helm.

Version tags are linked to the bundled version of helm, the most recent tags are
auto-updated weekly with the latest gcloud version.

Run the image:
```
docker run -it --rm soon/gke-helm sh
```

Use it in CI:
```
deploy:
  stage: deploy
  image: soon/gke-helm
  script:
    - gcloud auth ...
    - gcloud container ...
    - helm repo add project https://...
    - helm upgrade ...
```
