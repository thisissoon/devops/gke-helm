arr=$(echo $VERSIONS | tr "," "\n")

for x in $arr
do
    echo "Building \"$DOCKER_IMAGE:$x\""
    /kaniko/executor \
      --cache=false \
      --context "${CI_PROJECT_DIR}" \
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile" \
      --build-arg HELM_TARBALL=helm-$x-linux-amd64.tar.gz \
      --destination "${DOCKER_IMAGE}:${x}"
done
